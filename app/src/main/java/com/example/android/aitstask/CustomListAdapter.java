package com.example.android.aitstask;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Fatima Mostafa on 22-Feb-17.
 */

public class CustomListAdapter extends ArrayAdapter<ListModel> {


    public CustomListAdapter(Activity context, ArrayList<ListModel> listModels) {

        super(context, 0, listModels);
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        ListModel currentListModel = getItem(position);

        TextView titleTextView = (TextView) listItemView.findViewById(R.id.title_text_view);
        titleTextView.setText(currentListModel.getTitle());

        TextView contentTextView = (TextView) listItemView.findViewById(R.id.content_text_view);
        contentTextView.setText(currentListModel.getContent());

        ImageView imageView = (ImageView) listItemView.findViewById(R.id.image);

        return listItemView;

    }
}

